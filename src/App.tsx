import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import {store, persistor} from './store/index';
import NavigationRoot from './NavigationRoot';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer>
          <NavigationRoot />
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
};

export default App;
