import React from 'react';
import Login from './Pages/Login/Login';
import Register from './Pages/Register/Register';
import LoggedInApp from './Pages/LoggedInApp/LoggedInApp';
import {createStackNavigator} from '@react-navigation/stack';
import {enableScreens} from 'react-native-screens';
import {useSelector} from 'react-redux';
import {RootState} from './store/index';

enableScreens();

export type RootStackParamList = {
  Login: undefined;
  Register: undefined;
  LoggedInApp: undefined;
};

const Stack = createStackNavigator<RootStackParamList>();
const NavigationRoot = () => {
  const jwt = useSelector<RootState, string | undefined>(
    (state) => state.user.jwt,
  );

  return (
    <Stack.Navigator headerMode="none">
      {!jwt ? (
        <>
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Register" component={Register} />
        </>
      ) : (
        <Stack.Screen name="LoggedInApp" component={LoggedInApp} />
      )}
    </Stack.Navigator>
  );
};

export default NavigationRoot;
