import React, {useMemo, useRef, useCallback, useState} from 'react';
import {StyleSheet, View, LayoutRectangle} from 'react-native';
import {followsSequence, useInterval} from '../../../utils';
import {
  EffectType,
  getRandomSpellType,
  SpellWithCoords,
  Coord,
} from './types/Spell';
import {
  PanGestureHandler,
  PanGestureHandlerGestureEvent,
  PanGestureHandlerStateChangeEvent,
  State,
} from 'react-native-gesture-handler';
import SpellCard from './SpellCard';
import {gridSide} from './types/Spell';

const Board = () => {
  const [spells, setSpells] = useState<SpellWithCoords[]>([]);
  const [sequence, setSequence] = useState<Coord[]>([]);

  const spellId = useRef<number>(0);
  const boardRef = useRef<View | null>();
  const boardRect = useRef<LayoutRectangle | undefined>();
  const [height, setHeight] = useState<number | undefined>();

  const runSequence = useCallback(() => {
    setSequence((prevSequence) => {
      if (prevSequence.length < 2) {
        return [];
      }

      setSpells((prevSpells) => {
        const spellsToBeKept = prevSpells
          .filter(
            (sp) =>
              !prevSequence.some(
                (coord) => sp.x === coord.x && sp.y === coord.y,
              ),
          )
          .map((sp) => {
            const removedBellow = prevSequence.filter(
              (coordToRemove) =>
                coordToRemove.x === sp.x && coordToRemove.y < sp.y,
            ).length;

            return {...sp, y: sp.y - removedBellow};
          });

        return spellsToBeKept;
      });
      return [];
    });
  }, []);

  const generateNewSpell = useCallback(() => {
    setSpells((prevSpells) => {
      if (prevSpells.length === gridSide * gridSide) {
        return prevSpells;
      }

      let randomX: number | undefined;
      let lowestOpenY: number | undefined;
      do {
        randomX = Math.floor(Math.random() * gridSide);

        const allYs = Array.from(new Array(gridSide), (_, i) => i); //start from bottom up

        lowestOpenY = allYs.find(
          (y) => !prevSpells.some((sp) => sp.x === randomX && sp.y === y),
        );
      } while (lowestOpenY === undefined);

      return [
        ...prevSpells,
        {
          x: randomX,
          y: lowestOpenY,
          id: (++spellId.current).toString(),
          spell: {
            effectType: EffectType.DAMAGE,
            title: 'fireball',
            type: getRandomSpellType(),
          },
        },
      ];
    });
  }, []);

  const generate3Spells = useCallback(() => {
    generateNewSpell();
    generateNewSpell();
    generateNewSpell();
  }, [generateNewSpell]);

  useInterval(generate3Spells, 10000);

  const onPan = useCallback(
    ({nativeEvent}: PanGestureHandlerGestureEvent) => {
      setSequence((prevSequence) => {
        if (!boardRect.current) {
          return prevSequence;
        }

        const tileX = Math.floor(
          (nativeEvent.x / boardRect.current.height) * gridSide,
        );
        const tileY =
          gridSide -
          Math.floor((nativeEvent.y / boardRect.current.height) * gridSide) -
          1;

        if (
          !prevSequence.some(
            (coord) => coord.x === tileX && coord.y === tileY,
          ) &&
          (prevSequence.length === 0 ||
            (followsSequence(
              prevSequence[prevSequence.length - 1].x,
              prevSequence[prevSequence.length - 1].y,
              tileX,
              tileY,
            ) &&
              spells.find((sp) => sp.x === tileX && sp.y === tileY)?.spell
                .type ===
                spells.find(
                  (sp) =>
                    sp.x === prevSequence[prevSequence.length - 1].x &&
                    sp.y === prevSequence[prevSequence.length - 1].y,
                )?.spell.type)) &&
          spells.some((sp) => sp.x === tileX && sp.y === tileY)
        ) {
          return [...prevSequence, {x: tileX, y: tileY}];
        } else {
          return prevSequence;
        }
      });
    },
    [spells],
  );

  const onStateChange = useCallback(
    ({nativeEvent}: PanGestureHandlerStateChangeEvent) => {
      if (nativeEvent.state === State.END) {
        runSequence();
      }
    },
    [runSequence],
  );

  const signalLayoutChanged = useCallback(() => {
    if (boardRef.current) {
      boardRef.current?.measure((x, y, width, lHeight, pageX, pageY) => {
        boardRect.current = {
          height: lHeight,
          width,
          x: pageX,
          y: pageY,
        };
        setHeight(width);
      });
    }
  }, []);

  const setRef = useCallback(
    (ref: View | null) => {
      boardRef.current = ref;
    },
    [boardRef],
  );

  const tileHeight = useMemo(() => (height || 0) / gridSide, [height]);
  const tileWidth = useMemo(() => (height || 0) / gridSide, [height]);

  const styles = useMemo(
    () =>
      StyleSheet.create({
        row: {
          flexDirection: 'row',
        },
        boardStyle: {
          backgroundColor: 'grey',
          height,
          justifyContent: 'flex-end',
          overflow: 'hidden',
        },
        page: {
          flex: 1,
          justifyContent: 'center',
        },
      }),
    [height],
  );

  return (
    <View style={styles.page}>
      <PanGestureHandler
        onGestureEvent={onPan}
        onHandlerStateChange={onStateChange}>
        <View
          style={styles.boardStyle}
          onLayout={signalLayoutChanged}
          ref={setRef}>
          {spells.map((spell) => (
            <SpellCard
              key={spell.id}
              {...spell}
              selected={sequence.some(
                (coord) => coord.x === spell.x && coord.y === spell.y,
              )}
              height={tileHeight}
              width={tileWidth}
            />
          ))}
        </View>
      </PanGestureHandler>
    </View>
  );
};

export default Board;
