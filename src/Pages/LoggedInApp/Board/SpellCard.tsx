import React, {useEffect, useRef, useCallback, useMemo} from 'react';
import {Animated} from 'react-native';
import {GenericSpell, SpellType, gridSide, Coord} from './types/Spell';
import LottieView from 'lottie-react-native';

interface SpellCardProps extends Coord {
  spell: GenericSpell;
  selected: boolean;
  width: number | undefined;
  height: number | undefined;
}

const SpellCard: React.FC<SpellCardProps> = ({
  spell,
  selected,
  width,
  height,
  x,
  y,
}) => {
  const rotation = useRef(new Animated.Value(0)).current;
  const lottie = useRef<LottieView | undefined>();
  const yAnim = useRef(new Animated.Value(gridSide)).current;
  const xAnim = useRef(new Animated.Value(x)).current;

  const setLottieRef = useCallback((ref: LottieView) => {
    lottie.current = ref;
  }, []);

  useEffect(() => {
    Animated.timing(yAnim, {
      toValue: y,
      useNativeDriver: true,
    }).start();

    Animated.timing(xAnim, {
      toValue: x,
      useNativeDriver: true,
    }).start();
  }, [x, xAnim, yAnim, y]);

  useEffect(() => {
    if (selected) {
      // Animated.loop(
      //   Animated.sequence([
      //     Animated.timing(rotation, {
      //       toValue: -1,
      //       useNativeDriver: true,
      //       duration: shakeDuration,
      //     }),

      //     Animated.timing(rotation, {
      //       toValue: 1,
      //       useNativeDriver: true,
      //       duration: shakeDuration,
      //     }),
      //   ]),
      // ).start();

      lottie.current?.play();
    } else {
      // Animated.timing(rotation, {
      //   toValue: 0,
      //   useNativeDriver: true,
      // }).start();

      lottie.current?.pause();
      lottie.current?.reset();
    }
  });

  const styles = useMemo(
    () => ({
      width: width,
      height: height,
      position: 'absolute',
      left: 0,
      bottom: 0,
      transform: [
        {
          translateY: yAnim.interpolate({
            inputRange: [0, gridSide],
            outputRange: [0, -(height || 0) * gridSide],
          }),
        },
        {
          translateX: xAnim.interpolate({
            inputRange: [0, gridSide],
            outputRange: [0, (width || 0) * gridSide],
          }),
        },
        {
          rotate: rotation.interpolate({
            inputRange: [-1, 1],
            outputRange: ['-25deg', '25deg'],
          }),
        },
      ],
    }),
    [width, height, yAnim, rotation, xAnim],
  );

  return (
    <Animated.View style={styles} renderToHardwareTextureAndroid>
      {spell.type === SpellType.FIRE ? (
        <LottieView
          hardwareAccelerationAndroid
          cacheStrategy="strong"
          source={require('./spell_animations/fireball.json')}
          ref={setLottieRef}
          loop
        />
      ) : (
        <LottieView
          cacheStrategy="strong"
          hardwareAccelerationAndroid
          source={require('./spell_animations/tree.json')}
          ref={setLottieRef}
          speed={3}
          loop
        />
      )}
    </Animated.View>
  );
};

export default React.memo(SpellCard);
