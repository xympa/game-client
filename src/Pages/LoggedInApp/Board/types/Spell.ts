export enum SpellType {
  EARTH,
  FIRE,
}

export enum EffectType {
  HEAL,
  DAMAGE,
  MANA,
}

interface Effect {
  type: EffectType;
  amount: number;
}

export interface GenericSpell {
  type: SpellType;
  effectType: EffectType;
  title: string;
}

export const getRandomSpellType = () => {
  const random = Math.floor(
    (Math.random() * Object.keys(SpellType).length) / 2,
  );

  const type = random;

  return type;
};

export const gridSide = 4;

export type Coord = {
  x: number;
  y: number;
};

export interface SpellWithCoords extends Coord {
  spell: GenericSpell;
  id: string;
}
