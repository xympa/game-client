import React from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
import Modal from 'react-native-modal';

interface ChallengedModalProps {
  challenger: string | undefined;
}

const ChallengedModal: React.FC<ChallengedModalProps> = ({challenger}) => {
  return (
    <Modal coverScreen={false} isVisible={!!challenger}>
      <View>
        <Text>{`You have been challenged by ${challenger}`}</Text>
        <View style={styles.footer}>
          <Button onPress={() => {}} title="Deny" />
          <Button onPress={() => {}} title="Accept" />
        </View>
      </View>
    </Modal>
  );
};

export default ChallengedModal;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderRadius: 10,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 50,
  },
});
