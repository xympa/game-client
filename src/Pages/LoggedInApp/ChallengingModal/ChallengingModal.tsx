import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Modal from 'react-native-modal';

interface ChallengedModalProps {
  target: string | undefined;
}

const ChallengingModal: React.FC<ChallengedModalProps> = ({target}) => {
  return (
    <Modal style={} coverScreen={false} isVisible={!!target}>
      <View style={styles.container}>
        <Text>{`You are challenging ${target}`}</Text>
      </View>
    </Modal>
  );
};

export default ChallengingModal;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderRadius: 10,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 50,
  },
  modal: {
    justifyContent: 'center',
  },
});
