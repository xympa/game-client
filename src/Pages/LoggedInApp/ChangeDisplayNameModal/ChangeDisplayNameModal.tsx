import React, {useState} from 'react';
import {StyleSheet, Text, View, TextInput, Button} from 'react-native';
import Modal from 'react-native-modal';
import {useSelector} from 'react-redux';
import {RootState} from '../../../store/index';
import {WebSocket} from '../../../socket/useWebSocket';

const ChangeDisplayName = () => {
  const showChangeDisplayNameModal = useSelector<RootState, boolean>(
    (state) => state.app.mustChangeDisplayName,
  );

  const websocket = new WebSocket();

  const [displayName, setDisplayName] = useState<string>('');

  return (
    <Modal
      coverScreen={false}
      useNativeDriver
      isVisible={showChangeDisplayNameModal}>
      <View style={styles.container}>
        <Text>
          You're about to start your adventure as a mighty spell wielder, what
          will the world know it's hero by?
        </Text>
        <TextInput
          value={displayName}
          placeholder="Type out your name..."
          onChangeText={setDisplayName}
        />

        <Button
          title="change"
          onPress={() => {
            websocket.changeDisplayName(displayName);
          }}
        />
      </View>
    </Modal>
  );
};

export default ChangeDisplayName;

const styles = StyleSheet.create({
  container: {
    padding: 10,
    borderRadius: 10,
    backgroundColor: 'white',
  },
});
