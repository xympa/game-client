import React from 'react';
import {ScrollView, View} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {RootState} from '../../../store/index';
import {Player} from '../../../store/lobby/types';
import Header from '../../../components/Header/Header';
import LogoutIcon from '../../../assets/logout.svg';
import {logout} from '../../../store/userPreferences/actions';
import LobbyEntry from './LobbyEntry';
import ChallengedModal from '../ChallengedModal/ChallengedModal';
import {UserPreferencesState} from '../../../store/userPreferences/types';
import ChallengingModal from '../ChallengingModal/ChallengingModal';
import {AppState} from '../../../store/app/types';

const Lobby = () => {
  const players = useSelector<RootState, Player[]>(
    (state: RootState) => state.lobby.players,
  );

  const {displayName} = useSelector<RootState, UserPreferencesState>(
    (state: RootState) => state.user,
  );

  const {challengingUser, beingChallenged} = useSelector<RootState, AppState>(
    (state) => state.app,
  );

  const dispatch = useDispatch();

  return (
    <View>
      <ChallengedModal challenger={beingChallenged} />
      <ChallengingModal target={challengingUser} />
      <Header
        title={`Lobby (${displayName})`}
        leftContent={<LogoutIcon fill="black" height="50%" width="50%" />}
        leftContentOnPress={() => dispatch(logout())}
      />
      <ScrollView>
        {players.map((player) => (
          <LobbyEntry key={player.id} {...player} />
        ))}
      </ScrollView>
    </View>
  );
};

export default Lobby;
