import React, {useCallback} from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
import {useDispatch} from 'react-redux';
import {issueChallenge} from '../../../store/app/actions';

interface LobbyEntryProps {
  displayName: string;
  id: string;
}

const LobbyEntry: React.FC<LobbyEntryProps> = ({displayName, id}) => {
  const dispatch = useDispatch();

  const challenge = useCallback(() => {
    dispatch(issueChallenge(id, displayName));
  }, [id, displayName, dispatch]);

  return (
    <View style={styles.entry}>
      <Text>{displayName}</Text>
      <Button onPress={challenge} title="Challenge" />
    </View>
  );
};

export default React.memo(LobbyEntry);

const styles = StyleSheet.create({
  entry: {
    height: 32,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
