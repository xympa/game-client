import React, {useEffect} from 'react';
import Lobby from './Lobby/Lobby';
import Board from './Board/Board';
import {createStackNavigator} from '@react-navigation/stack';
import {WebSocket} from '../../socket/useWebSocket';
import ChangeDisplayName from './ChangeDisplayNameModal/ChangeDisplayNameModal';

const Stack = createStackNavigator();

const LoggedInApp = () => {
  useEffect(() => {
    const websocket = new WebSocket();

    return () => {
      websocket.destructor();
    };
  }, []);

  return (
    <>
      <ChangeDisplayName />
      <Stack.Navigator headerMode="none" initialRouteName="Lobby">
        <Stack.Screen name="Lobby" component={Lobby} />
        <Stack.Screen name="Game" component={Board} />
      </Stack.Navigator>
    </>
  );
};

export default LoggedInApp;
