import React, {useState, useCallback} from 'react';
import {ScrollView, StyleSheet, Button, Text, TextInput} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import {attemptLogin} from '../../store/userPreferences/actions';
import {useDispatch} from 'react-redux';
import {showDangerToast} from '../../utils';
import {RootStackParamList} from '../../NavigationRoot';

type ProfileScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Login'
>;

type Props = {
  navigation: ProfileScreenNavigationProp;
};

const Login: React.FC<Props> = ({navigation}) => {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const dispatch = useDispatch();

  const login = useCallback(async () => {
    try {
      await dispatch(attemptLogin({email, password}));
      console.log('did good');
    } catch (err) {
      showDangerToast('Wrong password');
    }
  }, [dispatch, email, password]);

  return (
    <ScrollView>
      <TextInput
        placeholder="email"
        value={email}
        style={styles.inputs}
        onChangeText={setEmail}
      />
      <TextInput
        placeholder="password"
        value={password}
        keyboardType="email-address"
        secureTextEntry
        style={styles.inputs}
        onChangeText={setPassword}
      />
      <Button title="Login" onPress={login} />
      <Text>OR</Text>
      <Button
        title="Register"
        onPress={() => navigation.navigate('Register')}
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  inputs: {
    textAlign: 'center',
  },
});

export default Login;
