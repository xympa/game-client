import React, {useState, useCallback} from 'react';
import {ScrollView, StyleSheet, Button, Text, TextInput} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import {attemptRegistration} from '../../store/userPreferences/actions';
import {useDispatch} from 'react-redux';
import {showDangerToast} from '../../utils';
import {RootStackParamList} from '../../NavigationRoot';

type ProfileScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Register'
>;

type Props = {
  navigation: ProfileScreenNavigationProp;
};

const Register: React.FC<Props> = ({navigation}) => {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');

  const dispatch = useDispatch();

  const register = useCallback(async () => {
    try {
      await dispatch(attemptRegistration({email, password}));
    } catch (err) {
      console.log('error');
      showDangerToast('Email already registered');
    }
  }, [dispatch, email, password]);

  return (
    <ScrollView contentContainerStyle={styles.content}>
      <TextInput
        placeholder="email"
        value={email}
        keyboardType="email-address"
        style={styles.inputs}
        onChangeText={setEmail}
      />
      <TextInput
        placeholder="password"
        value={password}
        secureTextEntry
        style={styles.inputs}
        onChangeText={setPassword}
      />
      <Button title="Login" onPress={register} />
      <Text>OR</Text>
      <Button title="Login" onPress={() => navigation.navigate('Login')} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  inputs: {
    textAlign: 'center',
  },
  content: {
    alignItems: 'center',
  },
});

export default Register;
