import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';

interface HeaderProps {
  title?: string;
  leftContent: React.ReactNode;
  leftContentOnPress?: () => void;
}

const Header: React.FC<HeaderProps> = ({
  title,
  leftContent,
  leftContentOnPress,
}) => {
  return (
    <View style={styles.header}>
      <Text>{title}</Text>
      {leftContent && (
        <TouchableWithoutFeedback onPress={leftContentOnPress}>
          <View style={styles.leftIcon}>{leftContent}</View>
        </TouchableWithoutFeedback>
      )}
    </View>
  );
};

export default React.memo(Header);

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    height: 48,
    maxHeight: 48,
    alignItems: 'center',
    elevation: 5,
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  leftIcon: {
    height: 48,
    maxHeight: 48,
    width: 48,
    minWidth: 48,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
