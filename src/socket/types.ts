import StrictEventEmitter from 'strict-event-emitter-types';

export enum SERVER_SENT_EVENT_TYPES {
  CONNECT = 'connect',
  DISCONNECT = 'disconnect',
  LOBBY_REFRESH = 'LOBBY_REFRESH',
  BAD_AUTH = 'BAD_AUTH',
  REQUEST_DISPLAY_NAME_SET = 'REQUEST_DISPLAY_NAME_SET',
  DISPLAY_NAME_CHANGED = 'SET_DISPLAY_NAME',
  DISPLAY_NAME_ALREADY_IN_USE = 'DISPLAY_NAME_ALREADY_IN_USE',
}

export enum CLIENT_SENT_EVENT_TYPES {
  LOGIN = 'LOGIN',
  SET_DISPLAY_NAME = 'SET_DISPLAY_NAME',
  ISSUE_CHALLENGE = 'ISSUE_CHALLENGE',
}

export interface LobbyPlayer {
  id: string;
  displayName: string;
}

interface ServerSentEvents {
  connect: void;
  lobbyRefresh: LobbyPlayer[];
  badAuth: void;
  requestDisplayNameSet: void;
  displayNameChanged: string;
  displayNameAlreadyInUse: void;
  youHaveBeenChallenged: string;
  challengeFailed: void;
}

export type ClientSentEvents = {
  login: string;
  setDisplayName: string;
  issueChallenge: string;
};

export type ClientEmitter = StrictEventEmitter<
  SocketIOClient.Socket,
  ServerSentEvents,
  ClientSentEvents
>;

export type ServerEmitter = StrictEventEmitter<
  SocketIOClient.Socket,
  ClientSentEvents,
  ServerSentEvents
>;
