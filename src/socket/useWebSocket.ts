import io from 'socket.io-client';
import {ClientEmitter} from './types';
import {store, RootState} from '../store/index';
import {refreshLobby} from '../store/lobby/actions';
import {
  setMustChangeDisplayName,
  receiveChallenge,
  cancelChallenge,
} from '../store/app/actions';
import {showDangerToast} from '../utils';
import {logout, setDisplayName} from '../store/userPreferences/actions';

export class WebSocket {
  private static websocket: ClientEmitter | undefined = undefined;

  constructor() {
    if (!WebSocket.websocket) {
      WebSocket.websocket = io('http://192.168.1.97:3000') as ClientEmitter;

      WebSocket.websocket.on('connect', () => {
        if (!WebSocket.websocket) {
          console.warn('BAILED OUT BEACUSE THERE WAS NO INSTANCE  ');
          return;
        }

        this.setupLobbyEvents();

        WebSocket.websocket.emit(
          'login',
          (store.getState() as RootState).user.jwt || '',
        );
      });
    }
  }

  private setupLobbyEvents() {
    if (!WebSocket.websocket) {
      console.warn('BAILED OUT BEACUSE THERE WAS NO INSTANCE  ');
      return;
    }

    WebSocket.websocket.on('badAuth', () => {
      store.dispatch(logout());
    });

    WebSocket.websocket.on('lobbyRefresh', (players) => {
      store.dispatch(refreshLobby(players));
    });

    WebSocket.websocket.on('requestDisplayNameSet', () => {
      store.dispatch(setMustChangeDisplayName(true));
    });

    WebSocket.websocket.on('displayNameChanged', (name) => {
      store.dispatch(setMustChangeDisplayName(false));
      store.dispatch(setDisplayName(name));
    });

    WebSocket.websocket.on('displayNameAlreadyInUse', () => {
      showDangerToast(
        "There is already a hero known by that name, let's not steal anyone's thunder.",
      );
    });

    WebSocket.websocket.on('youHaveBeenChallenged', (challengerName) => {
      store.dispatch(receiveChallenge(challengerName));
    });

    WebSocket.websocket.on('challengeFailed', () => {
      showDangerToast(
        'He denied your challenge, look at him, looking like he has better things to do than fight you...',
      );
      store.dispatch(cancelChallenge());
    });
  }

  changeDisplayName(newName: string) {
    WebSocket.websocket?.emit('setDisplayName', newName);
  }

  issueChallenge(targetId: string) {
    WebSocket.websocket?.emit('issueChallenge', targetId);
  }

  destructor() {
    WebSocket.websocket?.close();
    WebSocket.websocket = undefined;
  }
}
