import {WebSocket} from '../../socket/useWebSocket';
export const MUST_CHANGE_DISPLAY_NAME = 'MUST_CHANGE_DISPLAY_NAME';
export const RECEIVE_CHALLENGE = 'RECEIVE_CHALLENGE';
export const ISSUE_CHALLENGE = 'ISSUE_CHALLENGE';
export const CHALLENGE_CANCELED = 'CHALLENGE_CANCELED';

interface SetMustChangeDisplayNameAction {
  type: typeof MUST_CHANGE_DISPLAY_NAME;
  payload: boolean;
}

interface IssueChallengeAction {
  type: typeof ISSUE_CHALLENGE;
  payload: string;
}

interface ReceiveChallengeAction {
  type: typeof RECEIVE_CHALLENGE;
  payload: string;
}

interface ChallengeCanceledAction {
  type: typeof CHALLENGE_CANCELED;
}

export const setMustChangeDisplayName = (value: boolean): AppActionTypes => ({
  type: MUST_CHANGE_DISPLAY_NAME,
  payload: value,
});

export const issueChallenge = (
  id: string,
  displayName: string,
): AppActionTypes => {
  new WebSocket().issueChallenge(id);

  return {
    type: ISSUE_CHALLENGE,
    payload: displayName,
  };
};

export const receiveChallenge = (displayName: string): AppActionTypes => {
  return {
    type: RECEIVE_CHALLENGE,
    payload: displayName,
  };
};

export const cancelChallenge = (): AppActionTypes => {
  return {
    type: CHALLENGE_CANCELED,
  };
};

export type AppActionTypes =
  | SetMustChangeDisplayNameAction
  | IssueChallengeAction
  | ReceiveChallengeAction
  | ChallengeCanceledAction;
