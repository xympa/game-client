import {
  AppActionTypes,
  MUST_CHANGE_DISPLAY_NAME,
  ISSUE_CHALLENGE,
} from './actions';
import {AppState} from './types';
import {RECEIVE_CHALLENGE, CHALLENGE_CANCELED} from './actions';

const initialState: AppState = {
  mustChangeDisplayName: false,
  beingChallenged: undefined,
  challengingUser: undefined,
};

export function appReducer(
  state = initialState,
  action: AppActionTypes,
): AppState {
  switch (action.type) {
    case MUST_CHANGE_DISPLAY_NAME: {
      return {
        ...state,
        mustChangeDisplayName: action.payload,
      };
    }
    case ISSUE_CHALLENGE: {
      return {
        ...state,
        challengingUser: action.payload,
      };
    }
    case RECEIVE_CHALLENGE: {
      return {
        ...state,
        beingChallenged: action.payload,
      };
    }
    case CHALLENGE_CANCELED: {
      return {
        ...state,
        beingChallenged: undefined,
        challengingUser: undefined,
      };
    }
    default:
      return state;
  }
}
