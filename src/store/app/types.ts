export interface AppState {
  mustChangeDisplayName: boolean;
  challengingUser: string | undefined;
  beingChallenged: string | undefined;
}
