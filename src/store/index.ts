import {combineReducers, createStore, applyMiddleware} from 'redux';
import {lobbyReducer} from './lobby/reducer';
import {UserPreferencesReducer} from './userPreferences/reducer';
import thunk from 'redux-thunk';
import {persistStore, persistReducer} from 'redux-persist';
import {appReducer} from './app/reducer';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import {LobbyState} from './lobby/types';
import {UserPreferencesState} from './userPreferences/types';
import {AppState} from './app/types';
import AsyncStorage from '@react-native-community/async-storage';

const rootReducer = combineReducers<RootState>({
  lobby: lobbyReducer,
  user: UserPreferencesReducer,
  app: appReducer,
});

const persistConfig = {
  key: 'root',
  keyPrefix: '', // the redux-persist default is `persist:` which doesn't work with some file systems
  storage: AsyncStorage,
  whitelist: ['user'],
  stateReconciler: autoMergeLevel2,
};

//@ts-ignore

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(persistedReducer, applyMiddleware(thunk));

//@ts-ignore
export const persistor = persistStore(store);

export type RootState = {
  lobby: LobbyState;
  user: UserPreferencesState;
  app: AppState;
};
