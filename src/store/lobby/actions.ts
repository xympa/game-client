import {Player} from './types';
export const LOBBY_REFRESH = 'LOBBY_REFRESH';

interface LobbyRefreshAction {
  type: typeof LOBBY_REFRESH;
  payload: Player[];
}

export const refreshLobby = (players: Player[]): LobbyActionTypes => {
  return {
    type: LOBBY_REFRESH,
    payload: players,
  };
};

export type LobbyActionTypes = LobbyRefreshAction;
