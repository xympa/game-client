import {LobbyState} from './types';
import {LobbyActionTypes, LOBBY_REFRESH} from './actions';

const initialState: LobbyState = {
  players: [],
  lastUpdate: undefined,
};

export function lobbyReducer(
  state = initialState,
  action: LobbyActionTypes,
): LobbyState {
  switch (action.type) {
    case LOBBY_REFRESH: {
      return {
        ...state,
        players: action.payload,
        lastUpdate: new Date().toString(),
      };
    }
    default:
      return state;
  }
}
