export interface Player {
  id: string;
  displayName: string;
}

export interface LobbyState {
  lastUpdate: string | undefined;
  players: Player[];
}
