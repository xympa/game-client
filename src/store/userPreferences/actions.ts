import Axios from 'axios';
import {API_ROOT} from '../../constants';
import {AppThunk} from '../../utils';

export const LOGIN = 'LOGIN';
export const REGISTER = 'REGISTER';
export const LOGOUT = 'LOGOUT';
export const SET_DISPLAY_NAME = 'SET_DISPLAY_NAME';

interface LogoutAction {
  type: typeof LOGOUT;
}

interface SetDisplayNameAction {
  type: typeof SET_DISPLAY_NAME;
  payload: string;
}

interface LoginAction {
  type: typeof LOGIN;
  payload: {
    jwt: string;
  };
}

interface RegisterAction {
  type: typeof REGISTER;
  payload: {
    jwt: string;
  };
}

export type UserPreferencesActionTypes =
  | LoginAction
  | RegisterAction
  | LogoutAction
  | SetDisplayNameAction;

interface RegistrationDto {
  email: string;
  password: string;
}

interface RegistrationReturn {
  token: string;
}

const requestRegistration = (dto: RegistrationDto) =>
  Axios.post<RegistrationReturn>(`${API_ROOT}/authentication/sign-up`, dto);

export const attemptRegistration = (dto: RegistrationDto): AppThunk => async (
  dispatch,
) => {
  try {
    const {data} = await requestRegistration(dto);

    dispatch(registeredSuccessfully(data.token));

    return Promise.resolve();
  } catch (err) {
    return Promise.reject(err);
  }
};

const registeredSuccessfully = (jwt: string): UserPreferencesActionTypes => ({
  type: REGISTER,
  payload: {jwt},
});

const requestLogin = (dto: RegistrationDto) =>
  Axios.post<RegistrationReturn>(`${API_ROOT}/authentication/sign-in`, dto);

export const attemptLogin = (dto: RegistrationDto): AppThunk => async (
  dispatch,
) => {
  try {
    const {data} = await requestLogin(dto);
    dispatch(loggedInSuccessfully(data.token));

    return Promise.resolve();
  } catch (err) {
    return Promise.reject(err);
  }
};

const loggedInSuccessfully = (jwt: string): UserPreferencesActionTypes => {
  return {
    type: LOGIN,
    payload: {jwt},
  };
};

export const setDisplayName = (name: string): UserPreferencesActionTypes => {
  return {
    type: SET_DISPLAY_NAME,
    payload: name,
  };
};

export const logout = (): UserPreferencesActionTypes => ({
  type: LOGOUT,
});
