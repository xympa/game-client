import {UserPreferencesState} from './types';
import {
  UserPreferencesActionTypes,
  LOGIN,
  REGISTER,
  LOGOUT,
  SET_DISPLAY_NAME,
} from './actions';

const initialState: UserPreferencesState = {
  displayName: undefined,
  id: undefined,
  jwt: undefined,
};

export function UserPreferencesReducer(
  state = initialState,
  action: UserPreferencesActionTypes,
): UserPreferencesState {
  switch (action.type) {
    case LOGIN: {
      return {
        ...state,
        jwt: action.payload.jwt,
      };
    }
    case REGISTER: {
      return {
        ...state,
        jwt: action.payload.jwt,
      };
    }
    case SET_DISPLAY_NAME: {
      return {
        ...state,
        displayName: action.payload,
      };
    }
    case LOGOUT: {
      return initialState;
    }
    default:
      return state;
  }
}
