export interface UserPreferencesState {
  displayName: string | undefined;
  id: string | undefined;
  jwt: string | undefined;
}
