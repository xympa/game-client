import {LayoutRectangle} from 'react-native';
import {useEffect, useCallback, useRef} from 'react';
import {ThunkAction} from 'redux-thunk';
import {RootState} from './store';
import {Action} from 'redux';
import Toast from 'react-native-tiny-toast';

export const coordsIntersectRectangle = (
  coords: {x: number; y: number},
  rect: LayoutRectangle,
) => {
  return (
    coords.x >= rect.x &&
    coords.x <= rect.x + rect.width &&
    coords.y >= rect.y &&
    coords.y <= rect.y + rect.height
  );
};

export const followsSequence = (
  oldX: number,
  oldY: number,
  newX: number,
  newY: number,
) => {
  if (oldX === undefined || oldY === undefined) {
    return true;
  }

  const xAxisDiff = newX - oldX;
  const yAxisDiff = newY - oldY;

  return (
    (yAxisDiff === 1 && xAxisDiff === 0) ||
    (yAxisDiff === -1 && xAxisDiff === 0) ||
    (xAxisDiff === 1 && yAxisDiff === 0) ||
    (xAxisDiff === -1 && yAxisDiff === 0)
  );
};

// React hook for delaying calls with time
// returns callback to use for cancelling

export const useInterval = (
  callback: () => void, // function to call. No args passed.
  // if you create a new callback each render, then previous callback will be cancelled on render.
  timeout: number = 0, // delay, ms (default: immediately put into JS Event Queue)
): (() => void) => {
  const timeoutIdRef = useRef<number | undefined>(undefined);
  const cancel = useCallback(() => {
    const timeoutId = timeoutIdRef.current;
    if (timeoutId) {
      timeoutIdRef.current = undefined;
      clearInterval(timeoutId);
    }
  }, [timeoutIdRef]);

  useEffect(() => {
    timeoutIdRef.current = setInterval(callback, timeout);
    return cancel;
  }, [callback, timeout, cancel]);

  return cancel;
};

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

export const showDangerToast = (text: string) => {
  Toast.show(text, {
    containerStyle: {backgroundColor: 'red', zIndex: 9999, elevation: 21},
  });
};
